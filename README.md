## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Sample in GraphQL](#Sample-in-GraphQL)

## General info
A simple beginner GraphQl project, operating on movies db. GraphQL provide a website grafical interface, with a query field and result field to interact. Project work without frontend part, working on localhost admin/ and graphql/.

## Technologies
Project is created with:
* Python version: 3.8.2
* Django version: 2.2.3
* SQLite version: 3
* GraphQL version: 2.1
	
## Setup
To run this project, install it locally (in the virtual environment) using:

```
$ cd ../producthunt-project
$ pip install -r requirements.txt
$ python manage.py makemigrations
$ python manage.py migrate
$ python manage.py createsuperuser
$ python manage.py runserver
```

## Sample in GraphQL
```
query AllMovies {
  allMovies(first: 2){
    pageInfo {
      startCursor
      endCursor
      hasNextPage
      hasPreviousPage
    }
    edges{
      cursor
      node{
        id
        title
        year
        director{
          name
          surname
        }
      }
    }
  }
}
```